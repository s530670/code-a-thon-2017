(function () {
    'use strict';

    angular
        .module('app.contactus')
        .controller('contactUsController', ContactUs);

    ContactUs.$inject = ['$scope', '$state','$http'];
    function ContactUs($scope, $state,$http) {
        var vm = this;
        vm.title = 'ContactUs';
        vm.submitForm = submitForm;
        vm.userBack = angular.copy(vm.contact);
        function submitForm(){
            vm.successMsg = '';
            $http({
                method : "POST",
                url : "/contactUs",
                data : vm.contact
            }).then(function mySuccess(response) {
                vm.contact = angular.copy(vm.userBack);
                vm.successMsg = "Form submitted successfully."
            }, function myError(response) {
                vm.successMsg = '';
            });
            
        }
    }
})();
